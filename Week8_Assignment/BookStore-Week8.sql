create database BookStore;
use BookStore;

create table BookUsers(
username varchar(30) primary key  ,
password varchar(30)
);

create table Books(
id int primary key,
name varchar(30),
author varchar(30),
url varchar(15000)
);

create table ReadLater(
id int primary key,
name varchar(30),
author varchar(30),
url varchar(800)
);


create table Likelist(
id int primary key,
name varchar(30),
author varchar(30),
url varchar(800)
);



insert into Books values(1, "My Journey",  "Dr.A.P.J.Abdul Kalam","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQICpVY5xX7caaHRbA96fTIlp4xWoj0C9uUDA&usqp=CAU");
insert into Books values(2,"Heidi","Johanna Spyri","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Heidi-book-cover.jpg?resize=768,837&quality=82&strip=all");
insert into books values(3,"Anne of Green Gables","Lucy Maud Montgomery","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Anne-of-Green-Gables-book-cover.jpg?resize=768,1075&quality=82&strip=all");
insert into books values(4,"Black Beauty","Anna Sewell","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Black-Beauty.jpg?resize=768,1104&quality=82&strip=all");
insert into books values(5,"The Name of the Rose","Umberto Eco","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/The-Name-of-the-Rose.jpg?resize=768,1170&quality=82&strip=all");
insert into books values(6,"The Eagle Has Landed","Jack Higgins","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/The-Eagle-Has-Landed-book-cover.jpg?resize=768,1179&quality=82&strip=all");