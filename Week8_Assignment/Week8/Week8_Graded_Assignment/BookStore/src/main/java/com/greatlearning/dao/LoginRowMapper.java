package com.greatlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.greatlearning.bean.*;

public class LoginRowMapper implements RowMapper {

	@Override
	public Users mapRow(ResultSet rs, int rowNum) throws SQLException {
		Users users = new Users();
		users.setUsername(rs.getString("username"));
		users.setPassword(rs.getString("password"));
		return users;
	}

}
