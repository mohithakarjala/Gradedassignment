package com.greatlearning.service;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.greatlearning.dao.*;
import com.greatlearning.exception.*;

public class RegisterService {

	@Autowired
	RegisterDao dao ;

	public boolean registerUser(String username, String password) throws DuplicateKeyException {
		return dao.registerUser(username, password);
	}
}
