package com.greatlearning.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.greatlearning.bean.*;
import com.greatlearning.dao.*;
import com.greatlearning.service.*;

@Configuration
public class AppConfiguration {
	
	@Bean
    public DriverManagerDataSource ds() { 	
    	DriverManagerDataSource ds = new DriverManagerDataSource();
    	ds.setDriverClassName("com.mysql.cj.jdbc.Driver");;
    	ds.setUrl("jdbc:mysql://localhost:3306/BookStore");
    	ds.setUsername("root");
    	ds.setPassword("1234"); 	
    	return ds;
    	
    }
    @Bean
    public JdbcTemplate template() {
       	JdbcTemplate template = new JdbcTemplate();
    	template.setDataSource(ds());
    	return template;   	
    }

	@Bean
	@Scope(value = "prototype")
	public Books book() {
		return new Books();
	}
	
	@Bean
	@Scope(value = "prototype")
	public LikedBooks likedBook() {
		return new LikedBooks();
	}
	
	@Bean
	@Scope(value = "prototype")
	public ReadLaterBooks readLaterBook() {
		return new ReadLaterBooks();
	}
	
	@Bean
	public RegisterService service() {
		return new RegisterService();
	}
	
	@Bean
	public LoginService service1() {
		return new LoginService();
	}

	@Bean
	public BooksService service3() {
		return new BooksService();
	}
	
	@Bean
	public LikedBooksService service4() {
		return new LikedBooksService();
	}
	
	@Bean
	public ReadLaterService service5() {
		return new ReadLaterService();
	}
	
	@Bean
	public BooksDao dao() {
		return new BooksDao();
	}
	
	@Bean
	public LikedDao likedDao() {
		return new LikedDao();
	}
	
	@Bean
	public LoginDao loginDao() {
		return new LoginDao();
	}
		
	@Bean
	public ReadLaterDao readLaterDao() {
		return new ReadLaterDao();
	}
	
	@Bean
	public RegisterDao registerDao() {
		return new RegisterDao();
	}	
}
