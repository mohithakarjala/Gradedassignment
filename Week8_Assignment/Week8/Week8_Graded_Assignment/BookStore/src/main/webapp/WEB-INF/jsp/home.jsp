<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style>
body{
margin: 0;
padding: 0;
text-align: center;
}
div {
  border: 6px solid #949599;
  height: 150px;
  margin: 20px;
  padding: 20px;
  width: 400px;
}
a{
  font-size: 30px;
  color:#FF1493;
}

</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ page import="java.util.*"%>
	<%@ page import="com.greatlearning.bean.*"%>
<h1 style="font-size: 50px;color:#228B22;text-align:center;">Welcome To Book Store</h1>
<div>
	   <p>
			<a href="login">Login</a>
		</p>
		<p>
			<a href="register">Create Account (or) Register</a>
		</p>
	</div>
	<h2 style="font-size:30px;color:#4B0082;text-align:center;">Displaying Books With Login</h2>
  <table border="1" align="center" style="background-color:bisque;font-size:25px;border:2px solid Violet">
		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Author</b></th>
			<th><b>Image</b></th>
		</tr>
		
	<c:forEach var="i" items="${books}">
			<tr align = "table">
				<td><c:out value="${i.getId() }"></c:out></td>
				<td><c:out value="${i.getName() }"></c:out></td>
				<td><c:out value="${i.getAuthor() }"></c:out></td>
				<td><img src=<c:out value="${i.getUrl() }"></c:out>width="180" height="175"></td>
			</tr>
		</c:forEach>	
	
</body>
</html>