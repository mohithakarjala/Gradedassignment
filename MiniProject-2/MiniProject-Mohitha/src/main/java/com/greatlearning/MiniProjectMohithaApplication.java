package com.greatlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjectMohithaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectMohithaApplication.class, args);
		System.out.println("Server Running on Port number 8080");
	}

}
