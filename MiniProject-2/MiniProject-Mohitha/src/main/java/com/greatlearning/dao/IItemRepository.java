package com.greatlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.Item;

@Repository
public interface IItemRepository extends JpaRepository<Item, Integer> {

}
