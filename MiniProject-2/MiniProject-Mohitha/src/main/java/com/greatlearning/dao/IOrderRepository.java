package com.greatlearning.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.Order;

@Repository
public interface IOrderRepository extends JpaRepository<Order, Integer> {

}
