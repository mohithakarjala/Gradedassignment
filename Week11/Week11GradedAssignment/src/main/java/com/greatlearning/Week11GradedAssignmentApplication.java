package com.greatlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week11GradedAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week11GradedAssignmentApplication.class, args);
	}

}
