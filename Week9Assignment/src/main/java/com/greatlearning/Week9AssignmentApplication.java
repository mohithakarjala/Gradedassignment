package com.greatlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.greatlearning")
@EntityScan(basePackages = "com.greatlearning.bean")
@EnableJpaRepositories(basePackages = "com.greatlearning.dao")
public class Week9AssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week9AssignmentApplication.class, args);
		System.err.println("server running on port 9090");
	}

}
