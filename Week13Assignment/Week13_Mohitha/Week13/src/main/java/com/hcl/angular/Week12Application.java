package com.hcl.angular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week12Application {

	public static void main(String[] args) {
		SpringApplication.run(Week12Application.class, args);
		System.out.println("Server Running on Port Number 9090");
	}

}
