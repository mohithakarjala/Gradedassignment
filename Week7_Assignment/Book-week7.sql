create database Mohitha;
use Mohitha;

create table login(username varchar(20) not null,password varchar(20) not null);

create table registration(username varchar(20) not null,password varchar(20) not null,email varchar(20));


create table books(bookid int primary key,booktitle varchar(30),bookgenre varchar(30),image varchar(60));
insert into books values(1," The Names They Gave Us","Nature","The Names They Gave Us.jpg");
insert into books values(2,"Harry Potter","Fantasy Fiction","Harry Potter.jpg");
insert into books values(3,"Wings of Fire"," Autobiography","Wings of Fire.jpg");
insert into books values(4,"Book Is Funny","Humor","Book Is Funny.jpg");
insert into books values(5,"Six Of Crows","Fantasy","Six Of Crows.jpg");


select * from books;