package com.greatlearning.controller;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


import com.greatlearning.Dbresource.DbConnection;
import com.greatlearning.bean.Registration;
import com.greatlearning.dao.LoginDao;
import com.greatlearning.dao.RegistrationDao;
import com.greatlearning.service.LoginService;
import com.greatlearning.service.RegisterService;

@WebServlet("/RegistrationController")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RegisterService rs = new RegisterService();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter pw = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email=request.getParameter("email");
		Registration reg = new Registration();
		reg.setUname(username);
		reg.setPassword(password);
		reg.setEmail(email);
		
		RegisterService ls = new RegisterService();
		String res = ls.storeRegistration(reg);
						doGet(request, response);				
		pw.println(res);
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.include(request, response);

		
	}


}
	

	
